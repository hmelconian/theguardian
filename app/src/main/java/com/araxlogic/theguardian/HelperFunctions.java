package com.araxlogic.theguardian;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

import static android.content.Context.ACTIVITY_SERVICE;
import static android.content.Context.MODE_PRIVATE;

public class HelperFunctions {
    public static final String DB_NAME = "theguardian_db";
    private static final String SHAREDPREF_NAME = "TheGuardianPref";
    public static final String CHANNEL_ID = "COM_ARAXLOGIC_THEGUARDIAN_NOTIFICATION_CHANNEL";
    public static boolean isAppForeground = false;

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null) {
            return false;
        }
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable();
    }

    //converts a Drawable object to a base64-string
    public static String drawableToString(Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);    //PNG prevents from quality loss!
        byte[] bitmapdata = stream.toByteArray();
        return Base64.encodeToString(bitmapdata, Base64.DEFAULT);
    }

    //converts a base64-string to a Drawable object
    public static Drawable stringToDrawable(String base64String, Context context) {
        byte[] imageBytes = Base64.decode(base64String, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        return new BitmapDrawable(context.getResources(), decodedImage);
    }

    //required function for creating notifications in API 26+ (to be called as soon as the app starts)
    public static void createNotificationChannel(Context context) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = context.getString(R.string.channel_name);
            String description = context.getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
    }

    public static void putDatePref(String value, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(SHAREDPREF_NAME, MODE_PRIVATE).edit();
        editor.putString("key_first_name", value);
        editor.apply(); // commit changes in the background
    }

    public static String getDatePref(Context context) {
        return context.getSharedPreferences(SHAREDPREF_NAME, MODE_PRIVATE).getString("key_first_name", null);
    }

    public static void putLayoutmodePref(int value, Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences(SHAREDPREF_NAME, MODE_PRIVATE).edit();
        editor.putInt("key_layoutmode", value);
        editor.apply(); // commit changes in the background
    }

    public static int getLayoutmodePref(Context context, int defaultValue) {
        return context.getSharedPreferences(SHAREDPREF_NAME, MODE_PRIVATE).getInt("key_layoutmode", defaultValue);
    }

    //returns an approximate estimation of 20-item pages to be loaded to be safe before crashing
    public static int getMaxNumOfPages(Context context) {
        //in the line below, the 20 is the number of MBs used for the core app, and 12 is the number of MBs required approximately for every page of 20-items to be loaded
        return (((ActivityManager) context.getSystemService(ACTIVITY_SERVICE)).getMemoryClass() - 20) / 12;
    }
}