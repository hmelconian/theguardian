package com.araxlogic.theguardian.presenter;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class JSONParser {
    private JSONObject jObj = null;
    private String json = null;

    private static final String ENCODING = "UTF-8";

    public JSONParser() {}

    public JSONObject getJSONFromUrl(String urlString) {
        HttpURLConnection urlConnection = null;
        InputStream in = null;
        BufferedReader reader = null;
        InputStreamReader isr = null;
        try {
            URL url = new URL(urlString);
            urlConnection = (HttpURLConnection) url.openConnection();
            in = new BufferedInputStream(urlConnection.getInputStream());

            isr = new InputStreamReader(in, ENCODING);
            reader = new BufferedReader(isr, 8);
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }
            json = sb.toString();
        }
        catch (IOException e1) {
            e1.printStackTrace();
        }
        finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (isr != null) {
                try {
                    isr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                if (urlConnection != null && urlConnection.getInputStream() != null) {
                    urlConnection.getInputStream().close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        if (json != null) {
            // try parsing the string to a JSON object
            try {
                jObj = new JSONObject(json);
            } catch (JSONException e) {
                Log.e("JSON Parser", "Error parsing data " + e.toString());
            }
        }

        return jObj;    // return JSON String
    }

}