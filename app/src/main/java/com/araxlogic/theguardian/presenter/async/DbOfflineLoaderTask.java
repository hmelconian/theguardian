package com.araxlogic.theguardian.presenter.async;

import android.app.Activity;
import android.os.AsyncTask;
import android.view.View;

import androidx.room.Room;

import com.araxlogic.theguardian.HelperFunctions;
import com.araxlogic.theguardian.R;
import com.araxlogic.theguardian.model.News;
import com.araxlogic.theguardian.model.db.AppDatabase;
import com.araxlogic.theguardian.model.db.DbOfflineNews;
import com.araxlogic.theguardian.view.ScrollingActivity;

import java.util.List;

public class DbOfflineLoaderTask extends AsyncTask<Activity, Void, Activity> {
    private AppDatabase db;

    @Override
    protected Activity doInBackground(Activity... activity) {
        //populate the offlineDataset container with the items from the local database
        while (ScrollingActivity.offlineDataset.size() != 0) {    //purge the container (don't reinit for not breaking the adapter setup)
            ScrollingActivity.offlineDataset.remove();
        }
        db = Room.databaseBuilder(activity[0], AppDatabase.class, HelperFunctions.DB_NAME).build();
        List<DbOfflineNews> dbOfflineNews = db.newsDao().getAllOffline();
        DbOfflineNews dbOfflineNewsObject;
        for (int i = 0; i< dbOfflineNews.size(); ++i) {
            dbOfflineNewsObject = dbOfflineNews.get(i);
            ScrollingActivity.offlineDataset.add(new News(dbOfflineNewsObject.title, null, HelperFunctions.stringToDrawable(dbOfflineNewsObject.image, activity[0]), dbOfflineNewsObject.bodytext, dbOfflineNewsObject.sectionname));
        }
        return activity[0];
    }

    @Override
    protected void onPostExecute(Activity activity) {
        db.close();

        if (!HelperFunctions.isOnline(activity)) {
            if (ScrollingActivity.offlineDataset.size() == 0) {
                //don't show neither the main list nor the fav-list, but just the empty-list view
                activity.findViewById(R.id.main_recycler_view).setVisibility(View.GONE);
                activity.findViewById(R.id.empty_view).setVisibility(View.VISIBLE);
            } else {
                //show the main list with the saved items for offline viewing (and the fav-list) if there's any item in it
                activity.findViewById(R.id.main_recycler_view).setVisibility(View.VISIBLE);
                activity.findViewById(R.id.empty_view).setVisibility(View.GONE);
                ScrollingActivity.updateMainList();
            }
        }

    }
}