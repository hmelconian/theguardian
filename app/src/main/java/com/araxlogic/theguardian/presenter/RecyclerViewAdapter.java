package com.araxlogic.theguardian.presenter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.araxlogic.theguardian.R;
import com.araxlogic.theguardian.model.News;

import java.util.LinkedList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private LinkedList<News> mDataset;
    private Activity activity;

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewAdapter(LinkedList<News> myDataset, Activity activity) {
        mDataset = myDataset;
        this.activity = activity;
    }

    // Create new views (invoked by the layout manager)
    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        return new RecyclerViewHolder(v, this.activity);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        if (mDataset.get(position) != null) {
            String title = mDataset.get(position).getTitle();
            if (title == null) {
                title = "";
            }
            holder.getTitle().setText(title);

            holder.getImage().setImageDrawable(mDataset.get(position).getImage());

            String bodyText = mDataset.get(position).getBodytext();
            if (bodyText == null) {
                bodyText = "";
            }
            holder.getBodytext().setText(bodyText);

            String sectionnameText = mDataset.get(position).getSectionName();
            if (sectionnameText == null) {
                sectionnameText = "";
            }
            holder.getSectionname().setText(sectionnameText);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public void onViewRecycled(@NonNull RecyclerViewHolder holder) {
        super.onViewRecycled(holder);

        //recycle the image to avoid memory leaks and eventual app-crash
        holder.getImage().setImageDrawable(null);
    }

}