package com.araxlogic.theguardian.presenter.async;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.room.Room;

import com.araxlogic.theguardian.HelperFunctions;
import com.araxlogic.theguardian.model.db.AppDatabase;
import com.araxlogic.theguardian.view.ScrollingActivity;

public class InsertPinnedItemTask extends AsyncTask<Context, Void, Void> {
    private AppDatabase db;
    public static boolean isRunning;

    @Override
    protected Void doInBackground(Context... context) {
        synchronized (this) {
            isRunning = true;
        }

        db = Room.databaseBuilder(context[0], AppDatabase.class, HelperFunctions.DB_NAME).build();

        Log.d("DB", "saving pinned item");
        db.newsDao().insertPinnedNews(
                ScrollingActivity.dbItemForOperation.getTitle(),
                ScrollingActivity.dbItemForOperation.getBodytext(),
                HelperFunctions.drawableToString(ScrollingActivity.dbItemForOperation.getImage()),
                ScrollingActivity.dbItemForOperation.getSectionName()
        );

        return null;
    }

    @Override
    protected void onPostExecute(Void param) {
        db.close();
        ScrollingActivity.dbItemForOperation = null;    //free resources

        synchronized (this) {
            isRunning = false;
        }
    }
}