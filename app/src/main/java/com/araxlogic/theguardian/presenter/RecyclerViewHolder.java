package com.araxlogic.theguardian.presenter;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.araxlogic.theguardian.HelperFunctions;
import com.araxlogic.theguardian.R;
import com.araxlogic.theguardian.model.News;
import com.araxlogic.theguardian.view.ItemContentActivity;
import com.araxlogic.theguardian.view.ScrollingActivity;

import java.util.Objects;

public class RecyclerViewHolder extends RecyclerView.ViewHolder {
    private TextView title;
    private ImageView image;
    private TextView bodytext;
    private TextView sectionname;
    RecyclerViewHolder(final View itemview, final Activity activity) {
        super(itemview);
        title = itemview.findViewById(R.id.title);
        image = itemview.findViewById(R.id.image);
        bodytext = itemview.findViewById(R.id.bodytext);
        sectionname = itemview.findViewById(R.id.sectionname);

        itemview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //show the item content if the device is online or the item was saved for offline reading (NOTE: this is an artificial limitation to meet the requirements!)
                if (HelperFunctions.isOnline(activity) || ScrollingActivity.isInOfflineContainer(new News(title.getText().toString(), null, null, bodytext.getText().toString(), sectionname.getText().toString()))) {
                    Intent intent = new Intent(v.getContext(), ItemContentActivity.class);

                    //init the fields before launching the activity
                    ItemContentActivity.setItemTitle(title.getText().toString());
                    ItemContentActivity.setItemBodytext(bodytext.getText().toString());
                    ItemContentActivity.setItemImage(image.getDrawable());
                    ItemContentActivity.setItemSectionname(sectionname.getText().toString());

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {    //check LOLLIPOP instead of JELLY_BEAN because the transition used is limited itself to LOLLIPOP
                        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, itemview, Objects.requireNonNull(ViewCompat.getTransitionName(itemview)));
                        v.getContext().startActivity(intent, options.toBundle());
                    } else {
                        v.getContext().startActivity(intent);
                    }
                } else {
                    Toast.makeText(activity, activity.getResources().getString(R.string.pinned_item_unavailable_toast), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(TextView title) {
        this.title = title;
    }

    ImageView getImage() {
        return image;
    }

    public void setImage(ImageView image) {
        this.image = image;
    }

    public TextView getBodytext() {
        return bodytext;
    }

    public void setBodytext(TextView bodytext) {
        this.bodytext = bodytext;
    }

    public TextView getSectionname() {
        return sectionname;
    }

    public void setSectionname(TextView sectionname) {
        this.sectionname = sectionname;
    }
}