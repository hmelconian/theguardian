package com.araxlogic.theguardian.presenter.syncadapter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.araxlogic.theguardian.HelperFunctions;
import com.araxlogic.theguardian.R;
import com.araxlogic.theguardian.presenter.JSONParser;
import com.araxlogic.theguardian.view.ScrollingActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Handle the transfer of data between a server and an app, using the Android sync adapter framework.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    private static final String TAG = "SyncAdapter";

    private static final String RESPONSE = "response";
    private static final String RESULTS = "results";
    private static final String WEBPUBLICATIONDATE = "webPublicationDate";

    private static final String AUTHORITY = "com.araxlogic.theguardian.provider";     //this should be the same as in syncadapter.xml and have a format of no more than 4 dots-delimited!
    private static final String ACCOUNT_TYPE = "theguardian.araxlogic.com";    //this should be the same as in syncadapter.xml and have a format of no more than 4 dots-delimited!
    private static final String ACCOUNT = "Sync";
    private static final String SHARED_PREFS_FAILED_DEFAULT_INTERVAL = "300000";  //300 seconds = 5 min (will be used in extreme cases when failed to read a value from shared-prefs)

    // Global variables
    // Define a variable to contain a content resolver instance
    private ContentResolver mContentResolver;

    /**
     * Set up the sync adapter
     */
    SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        /*
         * If your app uses a content resolver, get an instance of it from the incoming Context
         */
        mContentResolver = context.getContentResolver();
    }

    /**
     * Set up the sync adapter. This form of the constructor maintains compatibility with Android 3.0 and later platform versions
     */
    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();
    }

    /*
     * Specify the code you want to run in the sync adapter. The entire
     * sync adapter runs in a background thread, so you don't have to set
     * up your own background processing.
     */
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
        syncData(getContext());     //the actual sync happens in this function
    }

    //this function can be called from any class to try to sync the app data once
    private static void syncData(Context context) {
        Log.d(TAG, "Inside syncData()");

        String urlTop = context.getString(R.string.sync_url) + "&from-date=" + HelperFunctions.getDatePref(context);
        JSONObject json = (new JSONParser()).getJSONFromUrl(urlTop);

        int size = 0;
        if (json == null) { //happens when there is no connection
            Log.w(TAG, "json is null!");
        } else {
            Log.d(TAG, "json = " + json);

            JSONArray jsonResultsArray;
            try {
                jsonResultsArray = json.getJSONObject(RESPONSE).getJSONArray(RESULTS);
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
                return;    //interrupt parsing the entire json since there is no object array at all
            }

            if (jsonResultsArray == null) { //if the object is still null
                return;    //interrupt parsing the entire json since there is no object array at all
            }

            size = jsonResultsArray.length();
            for (int i=0; i<size; ++i) {
                Log.d(TAG, " =>> i = " + i);

                JSONObject currentJsonObject;
                try {
                    currentJsonObject = jsonResultsArray.getJSONObject(i);
                } catch (JSONException e) {
                    Log.w(TAG, e.getMessage());
                    continue;   //skip parsing this item
                }

                String webPubDate = "";
                try {
                    webPubDate = currentJsonObject.getString(WEBPUBLICATIONDATE);
                } catch (JSONException e) {
                    Log.w(TAG, e.getMessage());
                }

                if (webPubDate != null) {   //update the first item date
                    ScrollingActivity.firstDate = webPubDate;
                    HelperFunctions.putDatePref(ScrollingActivity.firstDate, context);   //save the date string into shared-prefs for later use
                }

            }   //end of for() loop
        }

        if (size > 1) { //there was at least 1 new item processed in the above loop (size will always be at least 1 due to the dates' inclusive match)
            if (!HelperFunctions.isAppForeground) { //make sure the app (the ScrollingActivity) is not foreground
                //show a notification
                Log.d(TAG, "Generating a system notification!");

                // Create an explicit intent to be fired when the user taps on the notification
                Intent intent = new Intent(context, ScrollingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

                NotificationCompat.Builder builder = new NotificationCompat.Builder(context, HelperFunctions.CHANNEL_ID)
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle(context.getResources().getString(R.string.notification_title))
                        .setContentText(context.getResources().getString(R.string.notification_text))
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setContentIntent(pendingIntent)    //pendingIntent to fire when the user taps on it
                        .setAutoCancel(true)   //autoremove the notificatin when the user taps on it
                        .setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
                notificationManager.notify((int)System.currentTimeMillis(), builder.build());   //show the notification

                syncOff(context);   //turn off the sync-adapter to avoid multiple notifications being generated
            } else {    //the app (ScrollingActivity) is in foreground
                //fire a broadcast intent
                Log.d(TAG, "Firing a broadcast!");
                context.sendBroadcast(new Intent(ScrollingActivity.SyncBroadcastReceiver.INTENT_FILTER));
            }
        }
    }

    public static void syncOn(Context context) {
        Log.d(TAG, "inside syncOn()");
        Account newAccount = CreateSyncAccount(context);
        ContentResolver.setSyncAutomatically(newAccount, AUTHORITY, true);  //without this line the sync will be disabled and will not work until user manually switches it from the OS settings
        ContentResolver.addPeriodicSync(newAccount, AUTHORITY, Bundle.EMPTY, Long.parseLong(PreferenceManager.getDefaultSharedPreferences(context).getString("sync_frequency", SHARED_PREFS_FAILED_DEFAULT_INTERVAL))/1000);  //Turn on periodic syncing (parameter value in seconds)
//        ContentResolver.setIsSyncable(newAccount, AUTHORITY, 1);    //this will show the manual sync functionality in the OS settings if the sync is set as userVisible="true" in syncadapter.xml
    }

    private static void syncOff(Context context) {
        Log.d(TAG, "inside syncOff()");
        Account newAccount = CreateSyncAccount(context);
        ContentResolver.cancelSync(null, null); //cancel syncs for all accounts
        ContentResolver.removePeriodicSync(newAccount, AUTHORITY, Bundle.EMPTY);  //stop periodic syncs
//        ContentResolver.setIsSyncable(newAccount, AUTHORITY, 0);    //this will hide the manual sync functionality from the OS settings if the sync is set as userVisible="true" in syncadapter.xml
    }

    //this function may be called to trigger an immediate sync
    public static void syncOnceFast(Context context) {
        Log.d(TAG, "inside syncOnceFast()");

        //Pass the settings flags by inserting them in a bundle
        Bundle settingsBundle = new Bundle();

        //Forces a manual sync. The sync adapter framework ignores the existing settings, such as the flag set by setSyncAutomatically().
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);

        //Forces the sync to start immediately. If you don't set this, the system may wait several seconds before running
        //the sync request, because it tries to optimize battery use by scheduling many requests in a short period of time.
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

        //Request the sync for the default account, authority, and manual sync settings
        ContentResolver.requestSync(CreateSyncAccount(context), AUTHORITY, settingsBundle);
    }

    /**
     * Create a new dummy account for the sync adapter
     *
     * @param context The application context
     */
    private static Account CreateSyncAccount(Context context) {
        // Create the account type and default account
        Account newAccount = new Account(ACCOUNT, ACCOUNT_TYPE);
        // Get an instance of the Android account manager
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if (accountManager != null && accountManager.addAccountExplicitly(newAccount, null, null)) {
            /*
             * If you don't set android:syncable="true" in your <provider> element in the manifest,
             * then call context.setIsSyncable(account, AUTHORITY, 1) here.
             */
            return newAccount;
        } else {
            /*
             * The account exists or some other error occurred. Log this, report it,
             * or handle it internally.
             */
            return newAccount;
        }
    }

}