package com.araxlogic.theguardian.presenter.async;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.araxlogic.theguardian.HelperFunctions;
import com.araxlogic.theguardian.R;
import com.araxlogic.theguardian.model.News;
import com.araxlogic.theguardian.presenter.JSONParser;
import com.araxlogic.theguardian.view.ScrollingActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;

//the Boolean parameter defines if the items should be appended from the bottom or head of the list
public class NetworkLoadingTask extends AsyncTask<Activity, Void, Activity> {

    private static final String TAG = "NetworkLoadingTask";

    private static final String RESPONSE = "response";
    private static final String RESULTS = "results";
    private static final String FIELDS = "fields";
    private static final String HEADLINE = "headline";
    private static final String WEBURL = "webUrl";
    private static final String THUMBNAIL = "thumbnail";
    private static final String BODYTEXT = "bodyText";
    private static final String WEBPUBLICATIONDATE = "webPublicationDate";
    private static final String SECTIONNAME = "sectionName";

    public static boolean isRunning = false;

    private int containerSize;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        synchronized (this) {
            isRunning = true;
        }
    }

    @Override
    protected Activity doInBackground(Activity... pActivity) {
        Log.d(TAG, "inside NetworkLoadingTask.doInBackground()");

        JSONObject json;
        String urlTop;
        String urlBottom;

        if (ScrollingActivity.appendDirection) {
            urlBottom = pActivity[0].getString(R.string.main_url) + "&page-size=" + ScrollingActivity.PAGE_SIZE + "&page=" + (++ScrollingActivity.lastPageIndex);
            Log.d(TAG, "urlBottom = " + urlBottom);
            json = (new JSONParser()).getJSONFromUrl(urlBottom);
        } else {
            if (ScrollingActivity.firstPageIndexMinusOne < 1) {
                //in such a case query with page number = 1
                urlTop = pActivity[0].getString(R.string.main_url) + "&page-size=" + ScrollingActivity.PAGE_SIZE + "&page=1";
            } else {
                urlTop = pActivity[0].getString(R.string.main_url) + "&page-size=" + ScrollingActivity.PAGE_SIZE + "&page=" + (ScrollingActivity.firstPageIndexMinusOne--);
            }
            Log.d(TAG, "urlTop = " + urlTop);
            json = (new JSONParser()).getJSONFromUrl(urlTop);
        }

        if (json == null) { //happens when there is no connection
            Log.w(TAG, "json is null!");
        } else {
            Log.d(TAG, "json = " + json);

            JSONArray jsonResultsArray;
            try {
                jsonResultsArray = json.getJSONObject(RESPONSE).getJSONArray(RESULTS);
            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());
                return pActivity[0];    //interrupt parsing the entire json since there is no object array at all
            }

            if (jsonResultsArray == null) { //if the object is still null
                return pActivity[0];    //interrupt parsing the entire json since there is no object array at all
            }

            LinkedList<News> tempReverseDataset = new LinkedList<>();   //used for reversing the list of top-page
            int size = jsonResultsArray.length();
            for (int i=0; i<size; ++i) {
                Log.d(TAG, " =>> i = " + i);

                JSONObject currentJsonObject;
                try {
                    currentJsonObject = jsonResultsArray.getJSONObject(i);
                } catch (JSONException e) {
                    Log.w(TAG, e.getMessage());
                    continue;   //skip parsing this item
                }

                if (i == 0) {   //if parsing the first item, then need to remember the webPubDate of it
                    String webPubDate = "";
                    try {
                        webPubDate = currentJsonObject.getString(WEBPUBLICATIONDATE);
                    } catch (JSONException e) {
                        Log.w(TAG, e.getMessage());
                    }
                    ScrollingActivity.firstDate = webPubDate;
                    HelperFunctions.putDatePref(ScrollingActivity.firstDate, pActivity[0]);   //save the date string into shared-prefs for later use
                }

                String webUrl = "";
                try {
                    webUrl = currentJsonObject.getString(WEBURL);
                } catch (JSONException e) {
                    Log.w(TAG, e.getMessage());
                }

                String sectionName = "";
                try {
                    sectionName = currentJsonObject.getString(SECTIONNAME).trim();
                } catch (JSONException e) {
                    Log.w(TAG, e.getMessage());
                }

                JSONObject fields;
                try {
                    fields = currentJsonObject.getJSONObject(FIELDS);
                } catch (JSONException e) {
                    Log.w(TAG, e.getMessage());
                    continue;   //skip parsing this item's fileds object since it's in a bad state
                }

                if (fields == null) {
                    continue;   //skip parsing this item's fileds object since it's in a bad state
                }

                String headline = "";
                try {
                    headline = fields.getString(HEADLINE).trim();
                } catch (JSONException e) {
                    Log.w(TAG, e.getMessage());
                }

                String thumbnailUrl = "";
                try {
                    thumbnailUrl = fields.getString(THUMBNAIL);
                } catch (JSONException e) {
                    Log.w(TAG, e.getMessage());
                }

                String bodyText = "";
                try {
                    bodyText = fields.getString(BODYTEXT).trim();
                } catch (JSONException e) {
                    Log.w(TAG, e.getMessage());
                }

                Bitmap bitmap = null;
                if (thumbnailUrl != null) {
                    HttpURLConnection urlConnection = null;
                    InputStream inputStream = null;
                    try {
                        URL url = new URL(thumbnailUrl);
                        urlConnection = (HttpURLConnection) url.openConnection();
                        inputStream = urlConnection.getInputStream();
                        bitmap = BitmapFactory.decodeStream(inputStream);
                    } catch (Exception e) {
                        Log.w(TAG, e.getMessage());
                    }
                    finally{
                        if (inputStream != null) {
                            try {
                                inputStream.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        if (urlConnection != null) {
                            urlConnection.disconnect();
                        }
                    }
                }

                if (bitmap == null) {
                    continue;   //skip parsing this item's fileds object since it's in a bad state (no image)
                }

                News obj = new News(headline, webUrl, new BitmapDrawable(pActivity[0].getResources(), bitmap), bodyText, sectionName);
                if (ScrollingActivity.appendDirection) {
                    if (!ScrollingActivity.mainDataset.contains(obj)) {
                        ScrollingActivity.mainDataset.addLast(obj);

                        //immediately update the UI with the inserted item if it has less items than the page-size
                        if (ScrollingActivity.mainDataset.size() < ScrollingActivity.PAGE_SIZE) {
                            //updating dynamically when appending items at the bottom
                            pActivity[0].runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ScrollingActivity.updateMainList();
                                }
                            });
                        }
                    }
                } else {
                    if (!ScrollingActivity.mainDataset.contains(obj)) {
                        tempReverseDataset.addFirst(obj);
                    }
                }

            }   //end of for() loop

            //if was appending on top of the list, reverse insert into the mainDataset from top
            if (!ScrollingActivity.appendDirection) {
                containerSize = tempReverseDataset.size();
                for (int i=0; i<containerSize; ++i) {
                    ScrollingActivity.mainDataset.addFirst(tempReverseDataset.pollFirst());
                }
            }
        }

        return pActivity[0];
    }   //end of doInBackground()

    @Override
    protected void onPostExecute(Activity activity) {
        if (HelperFunctions.isAppForeground) {
            RecyclerView recyclerView = activity.findViewById(R.id.main_recycler_view);
            if (ScrollingActivity.mainDataset.size() == 0) {  //means failed to load for the first time for some reason (most likely of lost connection)
                recyclerView.setVisibility(View.GONE);
                activity.findViewById(R.id.fav_recycler_view).setVisibility(View.GONE);
                activity.findViewById(R.id.empty_view).setVisibility(View.VISIBLE);
            } else {
                recyclerView.setVisibility(View.VISIBLE);
                activity.findViewById(R.id.empty_view).setVisibility(View.GONE);
            }
            activity.findViewById(R.id.top_progressbar).setVisibility(View.GONE);
            activity.findViewById(R.id.bottom_progressbar).setVisibility(View.GONE);
            ScrollingActivity.updateMainList();

            if (!ScrollingActivity.appendDirection) {   //if was appending on top
                //scroll down to number of insert values, plus a bit more
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if (layoutManager != null) {
                    layoutManager.smoothScrollToPosition(recyclerView, new RecyclerView.State(), containerSize + 3);    //3 is an empiric value
                }
            }

        }

        synchronized (this) {
            isRunning = false;
        }

    }
}