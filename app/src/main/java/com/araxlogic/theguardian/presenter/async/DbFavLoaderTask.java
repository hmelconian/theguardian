package com.araxlogic.theguardian.presenter.async;

import android.app.Activity;
import android.os.AsyncTask;

import androidx.room.Room;

import com.araxlogic.theguardian.HelperFunctions;
import com.araxlogic.theguardian.model.News;
import com.araxlogic.theguardian.model.db.AppDatabase;
import com.araxlogic.theguardian.model.db.DbPinnedNews;
import com.araxlogic.theguardian.view.ScrollingActivity;

import java.util.List;

//populate the favDataset container with the items from the local database
public class DbFavLoaderTask extends AsyncTask<Activity, Void, Activity> {
    private AppDatabase db;

    @Override
    protected Activity doInBackground(Activity... activity) {
        while (ScrollingActivity.favDataset.size() != 0) {    //purge the container (don't reinit for not breaking the adapter setup)
            ScrollingActivity.favDataset.remove();
        }
        db = Room.databaseBuilder(activity[0], AppDatabase.class, HelperFunctions.DB_NAME).build();
        List<DbPinnedNews> dbPinnedNews = db.newsDao().getAllPinned();
        DbPinnedNews dbPinnedNewsObject;
        for (int i = 0; i < dbPinnedNews.size(); ++i) {
            dbPinnedNewsObject = dbPinnedNews.get(i);
            ScrollingActivity.favDataset.add(new News(dbPinnedNewsObject.title, null, HelperFunctions.stringToDrawable(dbPinnedNewsObject.image, activity[0]), dbPinnedNewsObject.bodytext, dbPinnedNewsObject.sectionname));
        }
        return activity[0];
    }

    @Override
    protected void onPostExecute(Activity activity) {
        db.close();
        ScrollingActivity.updateFavList(activity);
    }
}