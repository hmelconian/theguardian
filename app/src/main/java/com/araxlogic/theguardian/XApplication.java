package com.araxlogic.theguardian;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.araxlogic.theguardian.presenter.syncadapter.SyncAdapter;
import com.araxlogic.theguardian.view.ErrorActivity;

//this class will be mostly responsible for uncaught exception handling
public class XApplication extends Application {

    public PendingIntent pendingIntent;

    private Thread.UncaughtExceptionHandler androidDefaultUEH;

    private Thread.UncaughtExceptionHandler handler = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread thread, Throwable ex) {
            Log.e("uMediaroom", "Uncaught exception is: ", ex); //just a sample way of logging for now

            HelperFunctions.isAppForeground = false;
            SyncAdapter.syncOn(getApplicationContext());   //enable regular syncs on the background before closing the app

            //schedule a delayed call to start an activity via the pending intent
            AlarmManager mgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            if (mgr != null) {
                mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, pendingIntent);
            }

            System.exit(7); //argument other than 0 indicates abnormal termination
//            androidDefaultUEH.uncaughtException(thread, ex);    //otherwise, call the default uncaught-exception handler for default behaviour
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

        //init the pendingIntent for launching the error screen if an uncaught exception will be detected
        pendingIntent = PendingIntent.getActivity(
                getBaseContext(),
                0,
                new Intent(this, ErrorActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT);

        androidDefaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(handler);
    }

}
