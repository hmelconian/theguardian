package com.araxlogic.theguardian.view;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.araxlogic.theguardian.HelperFunctions;
import com.araxlogic.theguardian.R;
import com.araxlogic.theguardian.model.News;
import com.araxlogic.theguardian.presenter.HorizRecyclerViewAdapter;
import com.araxlogic.theguardian.presenter.RecyclerViewAdapter;
import com.araxlogic.theguardian.presenter.async.DbFavLoaderTask;
import com.araxlogic.theguardian.presenter.async.DbOfflineLoaderTask;
import com.araxlogic.theguardian.presenter.async.DeleteOfflineItemTask;
import com.araxlogic.theguardian.presenter.async.DeletePinnedItemTask;
import com.araxlogic.theguardian.presenter.async.InsertOfflineItemTask;
import com.araxlogic.theguardian.presenter.async.InsertPinnedItemTask;
import com.araxlogic.theguardian.presenter.async.NetworkLoadingTask;
import com.araxlogic.theguardian.presenter.syncadapter.SyncAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.LinkedList;

public class ScrollingActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView favRecyclerView;
    private static RecyclerView.Adapter mainAdapter;
    private static RecyclerView.Adapter favAdapter;
    public static boolean appendDirection; //true means append at bottom, false means append at top

    public static LinkedList<News> mainDataset;
    public static LinkedList<News> favDataset;
    public static LinkedList<News> offlineDataset;
    public static String firstDate;   //the date of the item residing at the head (first position) of the list

    private SyncBroadcastReceiver syncReceiver;

    private FloatingActionButton layoutSwitchFab;
    private LinearLayoutManager col1LayoutManager;
    private StaggeredGridLayoutManager col2LayoutManager;
    private StaggeredGridLayoutManager col3LayoutManager;
    private int layoutIndex;    //will define which layout to use
    public static long firstPageIndexMinusOne = 0;
    public static long lastPageIndex = 0;
    public static final int PAGE_SIZE = 20;
    public static int maxPages;    //will hold the max number of 20-item pages capable for holding once at a time in the recyclerView

    public static News dbItemForOperation;    //used as input parameter for AsyncTask classes to insert News objects into DB

    private RecyclerView.OnScrollListener onMainScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            if (newState == RecyclerView.SCROLL_STATE_IDLE && HelperFunctions.isOnline(ScrollingActivity.this)) {
                //check if the end/start of the list has reached
                if (!recyclerView.canScrollVertically(1) && !NetworkLoadingTask.isRunning) {    //the end reached by scrolling down!
                    //check if there's enough memory for loading a new page
                    if (mainDataset.size() + favDataset.size() > PAGE_SIZE * (maxPages - 1)) {
                        //drop the first page from the mainDataset and update it
                        for (int i=0; i<PAGE_SIZE; ++i) {
                            mainDataset.removeFirst();
                        }
                        ++firstPageIndexMinusOne;    //so that the loading of items from top will match same index (approximately...)
                        mainAdapter.notifyDataSetChanged();
                        recyclerView.scrollToPosition(mainAdapter.getItemCount()-1);    //scroll to the new bottom
                    }

                    appendDirection = true; //load more data at the bottom
                    findViewById(R.id.bottom_progressbar).setVisibility(View.VISIBLE);
                    new NetworkLoadingTask().execute(ScrollingActivity.this);
                } else {
                    if (!recyclerView.canScrollVertically(-1)) {    //the start reached by scrolling up
                        //check if there's enough memory for loading a new page
                        if (mainDataset.size() + favDataset.size() > PAGE_SIZE * (maxPages - 1)) {
                            //drop the last page from the mainDataset and update it
                            for (int i=0; i<PAGE_SIZE; ++i) {
                                mainDataset.removeLast();
                            }
                            --lastPageIndex;    //so that the loading of items from top will match same index (approximately...)
                            mainAdapter.notifyDataSetChanged();
                        }

                        appendDirection = false;    //load more data at the top
                        findViewById(R.id.top_progressbar).setVisibility(View.VISIBLE);
                        new NetworkLoadingTask().execute(ScrollingActivity.this);
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        maxPages = HelperFunctions.getMaxNumOfPages(this);

        //remove the blinking of the title-bar and status-bar upon activity switches
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setEnterTransition(null);
            getWindow().setExitTransition(null);
        }

        HelperFunctions.createNotificationChannel(this);

        recyclerView = findViewById(R.id.main_recycler_view);
        favRecyclerView = findViewById(R.id.fav_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);
        favRecyclerView.setHasFixedSize(true);

        // Set layout manager to position the items
        favRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        col1LayoutManager = new LinearLayoutManager(this);
        col2LayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL); // First param is number of columns and second param is orientation i.e Vertical or Horizontal
        col3LayoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);

        layoutIndex = HelperFunctions.getLayoutmodePref(this, 0);   //0 means to use 1-line layout by default if no shared-pref yet
        layoutSwitchFab = findViewById(R.id.fab);
        layoutSwitchFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutIndex = (layoutIndex+1)%3;    //looping over available layouts
                switchToLayout(layoutIndex);
                HelperFunctions.putLayoutmodePref(layoutIndex, view.getContext());  //save the layoutIndex into shared-prefs
            }
        });
        switchToLayout(layoutIndex);

        if (mainDataset == null) {
            mainDataset = new LinkedList<>();
        }
        if (favDataset == null) {
            favDataset = new LinkedList<>();
        }
        if (offlineDataset == null) {
            offlineDataset = new LinkedList<>();
        }

        if (HelperFunctions.isOnline(this)) {
            mainAdapter = new RecyclerViewAdapter(mainDataset, ScrollingActivity.this);
        } else {
            mainAdapter = new RecyclerViewAdapter(offlineDataset, ScrollingActivity.this);
            findViewById(R.id.bottom_progressbar).setVisibility(View.GONE);
        }
        recyclerView.setAdapter(mainAdapter);
        favAdapter = new HorizRecyclerViewAdapter(favDataset, ScrollingActivity.this);
        favRecyclerView.setAdapter(favAdapter);

        firstDate = null;
        HelperFunctions.putDatePref(null, this);   //save the date string into shared-prefs for later use

        new DbFavLoaderTask().execute(this);    //load the list of fav-items no matter whether online or offline
        if (HelperFunctions.isOnline(this)) {   //load the online feed
            Toast.makeText(this, getResources().getString(R.string.loading_popup), Toast.LENGTH_SHORT).show();
            firstPageIndexMinusOne = 0;     //ensure the loading of the first page when the activity is recreated!
            findViewById(R.id.top_progressbar).setVisibility(View.VISIBLE);
            appendDirection = true; //append from the bottom
            lastPageIndex = 0;  //this line is required to make sure the first page is queried, otherwise it wont via notification redirection
            new NetworkLoadingTask().execute(ScrollingActivity.this);   //load the first page
            SyncAdapter.syncOn(this);   //enable regular syncs on the background
        }
        new DbOfflineLoaderTask().execute(this);    //load and init the offline container from DB no matter online or not
    }

    @Override
    protected void onPause() {
        super.onPause();

        HelperFunctions.isAppForeground = false;

        recyclerView.suppressLayout(true);
        favRecyclerView.suppressLayout(true);
        recyclerView.removeOnScrollListener(onMainScrollListener);

        unregisterReceiver(syncReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        HelperFunctions.isAppForeground = true;
        if (!NetworkLoadingTask.isRunning) {
            findViewById(R.id.top_progressbar).setVisibility(View.GONE);
            findViewById(R.id.bottom_progressbar).setVisibility(View.GONE);
        }

        recyclerView.suppressLayout(false);
        favRecyclerView.suppressLayout(false);

        updateMainList();
        updateFavList(this);    //necessary for updating the list when coming back from the ItemContentActivity

        recyclerView.addOnScrollListener(onMainScrollListener);

        //register broadcast receiver to recieve sync updates
        syncReceiver = new SyncBroadcastReceiver();
        registerReceiver(syncReceiver, new IntentFilter(SyncBroadcastReceiver.INTENT_FILTER));
    }

    public static void updateMainList() {
        mainAdapter.notifyDataSetChanged();
    }

    public static void updateFavList(Activity activity) {
        if (favDataset != null && favDataset.size() > 0) {
            ((ScrollingActivity)activity).favRecyclerView.setVisibility(View.VISIBLE);
        } else {
            ((ScrollingActivity)activity).favRecyclerView.setVisibility(View.GONE);
        }
        favAdapter.notifyDataSetChanged();
    }

    public static boolean isInFavContainer(News item) {
        return favDataset.contains(item);
    }

    public static boolean isInOfflineContainer(News item) {
        if (offlineDataset == null || offlineDataset.size() == 0) {
            return false;
        }
        return offlineDataset.contains(item);
    }

    //4 functions called from the ItemContentActivity (add/remove item into the fav/offline containers and DB)
    public static void addIntoFavContainer(News item, Context context) {
        favDataset.add(item);
        dbItemForOperation = item;
        new InsertPinnedItemTask().execute(context);
    }
    public static void removeFromFavContainer(News item, Context context) {
        favDataset.remove(item);
        dbItemForOperation = item;
        new DeletePinnedItemTask().execute(context);
    }
    public static void addIntoOfflineContainer(News item, Context context) {
        offlineDataset.add(item);
        if (!HelperFunctions.isOnline(context)) {
            mainAdapter.notifyDataSetChanged(); //in case browsing in offline mode
        }
        dbItemForOperation = item;
        new InsertOfflineItemTask().execute(context);
    }
    public static void removeFromOfflineContainer(News item, Context context) {
        offlineDataset.remove(item);
        if (!HelperFunctions.isOnline(context)) {
            mainAdapter.notifyDataSetChanged(); //in case browsing in offline mode
        }
        dbItemForOperation = item;
        new DeleteOfflineItemTask().execute(context);
    }

    //nested broadcast receiver class to call the UI update function(s)
    public class SyncBroadcastReceiver extends BroadcastReceiver {

        public static final String INTENT_FILTER = "com.araxlogic.theguardian.UPDATE_INTENT";

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("SyncBroadcastReceiver", "Intercepting a broadcast!");

            //check if the top of the list was downloaded from the first page of the feed
            if (firstPageIndexMinusOne == 0) {  //then do same as scrolling up - copy-pasted code below
                //check if there's enough memory for loading a new page
                if (mainDataset.size() + favDataset.size() > PAGE_SIZE * (maxPages - 1)) {
                    //drop the last page from the mainDataset and update it
                    for (int i=0; i<PAGE_SIZE; ++i) {
                        mainDataset.removeLast();
                    }
                    --lastPageIndex;    //so that the loading of items from top will match same index (approximately...)
                    mainAdapter.notifyDataSetChanged();
                }

                appendDirection = false;    //load more data at the top
                findViewById(R.id.top_progressbar).setVisibility(View.VISIBLE);
                new NetworkLoadingTask().execute(ScrollingActivity.this);
            }
        }
    }

    private void switchToLayout(int layoutIndex) {
        switch (layoutIndex) {
            case 2:
                recyclerView.setLayoutManager(col3LayoutManager);
                layoutSwitchFab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.onecol));   //will switch to 1-col if tapped
                break;
            case 1:
                recyclerView.setLayoutManager(col2LayoutManager);
                layoutSwitchFab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.threecol)); //will switch to 3-col if tapped
                break;
            case 0:
            default:
                recyclerView.setLayoutManager(col1LayoutManager);
                layoutSwitchFab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.twocol));   //will switch to 2-col if tapped
        }

        //hide and show the button to avoid the bug of not showing the button icon after it's getting redisplayed by the user action
        //source: https://stackoverflow.com/a/56545000/4410376
        layoutSwitchFab.hide();
        layoutSwitchFab.show();
    }

}