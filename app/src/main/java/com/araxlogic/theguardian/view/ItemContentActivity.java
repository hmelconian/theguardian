package com.araxlogic.theguardian.view;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Layout;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.araxlogic.theguardian.R;
import com.araxlogic.theguardian.model.News;
import com.araxlogic.theguardian.presenter.async.DeleteOfflineItemTask;
import com.araxlogic.theguardian.presenter.async.DeletePinnedItemTask;
import com.araxlogic.theguardian.presenter.async.InsertOfflineItemTask;
import com.araxlogic.theguardian.presenter.async.InsertPinnedItemTask;

public class ItemContentActivity extends AppCompatActivity {

    //member fields to be populated by the caller activity
    private static String title;
    private static String bodytext;
    private static Drawable image;
    private static String sectionname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_content);

        //remove the blinking of the title-bar and status-bar upon activity switches
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setEnterTransition(null);
            getWindow().setExitTransition(null);
        }

        if (image != null) {
            ((ImageView) findViewById(R.id.image)).setImageDrawable(image);
        }

        if(bodytext != null) {
            ((TextView)findViewById(R.id.bodytext)).setText(bodytext);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                ((TextView) findViewById(R.id.bodytext)).setJustificationMode(Layout.JUSTIFICATION_MODE_INTER_WORD);
            }
        }

        if(title != null) {
            ((TextView)findViewById(R.id.title)).setText(title);
        }

        if(sectionname != null) {
            ((TextView)findViewById(R.id.sectionname)).setText(sectionname);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);

        News news = new News(title, null, image, bodytext, sectionname);
        if (ScrollingActivity.isInFavContainer(news)) {
            menu.findItem(R.id.action_pin).setIcon(android.R.drawable.btn_star_big_on);
        } else {
            menu.findItem(R.id.action_pin).setIcon(android.R.drawable.btn_star_big_off);
        }
        if (ScrollingActivity.isInOfflineContainer(news)) {
            menu.findItem(R.id.action_save).setIcon(android.R.drawable.ic_menu_delete);
        } else {
            menu.findItem(R.id.action_save).setIcon(android.R.drawable.ic_menu_save);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        News newsItem = new News(title, null, image, bodytext, sectionname);
        switch(id) {
            case R.id.action_pin:
                if (!InsertPinnedItemTask.isRunning && !DeletePinnedItemTask.isRunning) {
                    if (ScrollingActivity.isInFavContainer(newsItem)) {
                        ScrollingActivity.removeFromFavContainer(newsItem, this);
                        item.setIcon(android.R.drawable.btn_star_big_off);
                    } else {
                        ScrollingActivity.addIntoFavContainer(newsItem, this);
                        item.setIcon(android.R.drawable.btn_star_big_on);
                    }
                }
                return true;
            case R.id.action_save:
                if (!InsertOfflineItemTask.isRunning && !DeleteOfflineItemTask.isRunning) {
                    if (ScrollingActivity.isInOfflineContainer(newsItem)) {
                        ScrollingActivity.removeFromOfflineContainer(newsItem, this);
                        item.setIcon(android.R.drawable.ic_menu_save);
                    } else {
                        ScrollingActivity.addIntoOfflineContainer(newsItem, this);
                        item.setIcon(android.R.drawable.ic_menu_delete);
                    }
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static void setItemTitle(String title) {
        ItemContentActivity.title = title;
    }

    public static void setItemBodytext(String bodytext) {
        ItemContentActivity.bodytext = bodytext;
    }

    public static void setItemImage(Drawable image) {
        ItemContentActivity.image = image;
    }

    public static void setItemSectionname(String sectionname) {
        ItemContentActivity.sectionname = sectionname;
    }
}