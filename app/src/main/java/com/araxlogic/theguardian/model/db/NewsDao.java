package com.araxlogic.theguardian.model.db;

import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

@Dao
public interface NewsDao {
    @Query("SELECT * FROM DbOfflineNews")
    List<DbOfflineNews> getAllOffline();

    @Query("SELECT * FROM DbPinnedNews")
    List<DbPinnedNews> getAllPinned();

    @Query("INSERT INTO DbOfflineNews(title, bodytext, image, sectionname) VALUES (:newsTitle, :newsBodytext, :newsImage, :sectionname)")
    void insertOfflineNews(String newsTitle, String newsBodytext, String newsImage, String sectionname);

    @Query("INSERT INTO DbPinnedNews(title, bodytext, image, sectionname) VALUES (:newsTitle, :newsBodytext, :newsImage, :sectionname)")
    void insertPinnedNews(String newsTitle, String newsBodytext, String newsImage, String sectionname);

    @Query("DELETE FROM DbOfflineNews")
    void deleteAllOfflineItems();

    @Query("DELETE FROM DbPinnedNews")
    void deleteAllPinnedItems();

    @Query("DELETE FROM DbOfflineNews WHERE title LIKE '%' || :title || '%' AND bodytext LIKE '%' || :bodytext || '%' AND sectionname LIKE '%' || :sectionname || '%'")
    void deleteOfflineItem(String title, String bodytext, String sectionname);

    @Query("DELETE FROM DbPinnedNews WHERE title LIKE '%' || :title || '%' AND bodytext LIKE '%' || :bodytext || '%' AND sectionname LIKE '%' || :sectionname || '%'")
    void deletePinnedItem(String title, String bodytext, String sectionname);
}