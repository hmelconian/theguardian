package com.araxlogic.theguardian.model;

import android.graphics.drawable.Drawable;

public class News {
    private String title;
    private String url;     //TODO: get rid of this since no more used
    private Drawable image;
    private String bodytext;
    private String sectionName;

    @Override
    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!(other instanceof News))return false;
        News otherNews = (News)other;

        return otherNews.getTitle().equals(title)
                && otherNews.getSectionName().equals((sectionName));
//                && otherNews.getUrl().equals(url);
    }

    public News(String title, String url, Drawable image, String bodytext, String sectionName) {
        this.title = title;
        this.url = url;
        this.image = image;
        this.bodytext = bodytext;
        this.sectionName = sectionName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public String getBodytext() {
        return bodytext;
    }

    public void setBodytext(String bodytext) {
        this.bodytext = bodytext;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }
}
