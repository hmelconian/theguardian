package com.araxlogic.theguardian.model.db;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class DbOfflineNews {
    @PrimaryKey(autoGenerate = true)
    int uid;

    @ColumnInfo(name = "title")
    public String title;

    @ColumnInfo(name = "bodytext")
    public String bodytext;

    @ColumnInfo(name = "image")
    public String image;

    @ColumnInfo(name = "sectionname")
    public String sectionname;
}