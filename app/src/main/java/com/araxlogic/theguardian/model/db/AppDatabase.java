package com.araxlogic.theguardian.model.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {DbOfflineNews.class, DbPinnedNews.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract NewsDao newsDao();
}